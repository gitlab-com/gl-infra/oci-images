# OCI Images

This is a **monorepo** for all GitLab custom container images.

## Quick Start

### Adding A New Image

  1. Add a top-level directory to the repository with the name of your image.
  1. If your image has different falvors, add a subdirectory for each flavor.
  1. Then, add your `Dockerfile` and other supporting files.
  1. Add an entry to `.images-matrix` block in `.gitlab-ci` for each image.
